<?php 
// -----------------------------------------------------------------------------
// Shortcode for Phone Number Link - Pulls from ACF
// -----------------------------------------------------------------------------
add_shortcode('location_phone_number', function () {
	return '
		<a href="' . get_field('phone_number_link', 'option') . '"><span itemprop="telephone">' . get_field('location_phone', 'option') . '</span></a>';
	});

// -----------------------------------------------------------------------------
// Shortcode for Address - Pulls from ACF
// -----------------------------------------------------------------------------
add_shortcode('location_address', function () {
	return '
		<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="nap-info">
			<span itemprop="streetAddress">' . get_field('location_street_address', 'option') . '</span></br>
			<span itemprop="addressLocality">' . get_field('location_city', 'option') . '</span>,            
            <span itemprop="addressRegion">' . get_field('location_state', 'option') . '</span>
            <span itemprop="postalCode">' . get_field('location_zip_code', 'option') . '</span>
        </div>';
	});
	
// -----------------------------------------------------------------------------
// Shortcode for Email Link - Pulls from ACF
// -----------------------------------------------------------------------------
add_shortcode('location_email', function () {
	return '
		<a href="mailto:' . get_field('location_email_address', 'option') . '" itemprop="email">' . get_field('location_email_address', 'option') . '</a>';
	});
?>