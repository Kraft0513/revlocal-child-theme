<?php
	// Stop TinyMCE from auto-formatting WYSIWYG HTML
	remove_filter ('the_content',  'wpautop');
	remove_filter ('comment_text', 'wpautop');

	//Import Shortcode Functionality - from shortcodes.php file
	include 'shortcodes.php';

	// Register Bootstrap Navigation Walker
	require_once('wp_bootstrap_navwalker.php');

	// Custom Menus
	function register_menus() {
			register_nav_menus(
				array(
				'primary' => __( 'Bootstrap Menu', 'infinityhaver' ),
				'menu2' => __( 'menu2' )
			)
		);
	}
	add_action( 'init', 'register_menus' );

	// Register Shop Sidebar
	if ( ! function_exists( 'shop_sidebar' ) ) {

	function shop_sidebar() {

		$args = array(
			'id'            => 'shop-sidebar',
			'name'          => 'Shop Sidebar',
			'description'   => 'This is the sidebar for the shop page.',
		);
		register_sidebar( $args );

	}

	// Hook into the 'widgets_init' action
	add_action( 'widgets_init', 'shop_sidebar' );

	}

	// Register & Enqueue Scripts & Styles
	function register_styles_n_scripts() {
	// Keep scrits and style from loading in admin dashboard
	if ( ! is_admin() ) {

			// Stylesheets
			wp_register_style('fontawesome', get_template_directory_uri() . '/../Divi-Child/css/font-awesome.min.css', null, null, 'all' );
			wp_enqueue_style( 'fontawesome' );

			wp_register_style('bootstrap_style', get_template_directory_uri() . '/../Divi-Child/css/bootstrap.min.css', null, null, 'all' );
			wp_enqueue_style( 'bootstrap_style' );

			// Scripts
			wp_register_script( 'bootstrap_script', get_stylesheet_directory_uri() . '/../Divi-Child/js/bootstrap.min.js', array( 'jquery' ), null, false );
			wp_enqueue_script( 'bootstrap_script' );

			wp_register_script( 'script', get_stylesheet_directory_uri() . '/../Divi-Child/js/script.js', array( 'jquery' ), null, true );
			wp_enqueue_script( 'script' );
		}
	}
	add_action( 'init', 'register_styles_n_scripts' );